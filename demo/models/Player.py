from django.db import models
from .Base import Base

#======================================================================
# 
# Encapsulates data for model Player
#
# @author 
#
#======================================================================

#======================================================================
# Class Player Declaration
#======================================================================
class Player (Base):

#======================================================================
# attribute declarations
#======================================================================
	name = models.CharField(max_length=200, null=True)
	dateOfBirth = models.CharField(max_length=64, null=True)
	height = models.CharField(max_length=64, null=True)
	isProfessional = models.BooleanField(null=True)

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.name
		str = str + self.dateOfBirth
		str = str + self.height
		str = str + self.isProfessional
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Player";
    
	def objectType(self):
		return "Player";
