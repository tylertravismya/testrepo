from django.db import models
from .Base import Base

#======================================================================
# 
# Encapsulates data for model Alley
#
# @author 
#
#======================================================================

#======================================================================
# Class Alley Declaration
#======================================================================
class Alley (Base):

#======================================================================
# attribute declarations
#======================================================================
	name = models.CharField(max_length=200, null=True)
	leagues = models.ManyToManyField('League',  blank=True, related_name='+')
	tournaments = models.ManyToManyField('Tournament',  blank=True, related_name='+')
	lanes = models.ManyToManyField('Lane',  blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.name
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Alley";
    
	def objectType(self):
		return "Alley";
