from django.db import models
from .Base import Base

#======================================================================
# 
# Encapsulates data for model Game
#
# @author 
#
#======================================================================

#======================================================================
# Class Game Declaration
#======================================================================
class Game (Base):

#======================================================================
# attribute declarations
#======================================================================
	frames = models.IntegerField(null=True)
	player = models.OneToOneField('Player', on_delete=models.CASCADE, null=True, blank=True, related_name='+')

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.frames
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Game";
    
	def objectType(self):
		return "Game";
