from django.db import models
from .Base import Base

#======================================================================
# 
# Encapsulates data for model Lane
#
# @author 
#
#======================================================================

#======================================================================
# Class Lane Declaration
#======================================================================
class Lane (Base):

#======================================================================
# attribute declarations
#======================================================================
	number = models.IntegerField(null=True)

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.number
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Lane";
    
	def objectType(self):
		return "Lane";
