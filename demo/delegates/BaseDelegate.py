from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from demo.models.Base import Base
from demo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Base
#
# @author 
#
#======================================================================

#======================================================================
# Class BaseDelegate Declaration
#======================================================================
class BaseDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, baseId ):
		try:	
			base = Base.objects.filter(id=baseId)
			return base.first();
		except Base.DoesNotExist:
			raise ProcessingError("Base with id " + str(baseId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, base):
		for model in serializers.deserialize("json", base):
			model.save()
			return model;

	def create(self, base):
		base.save()
		return base;

	def saveFromJson(self, base):
		for model in serializers.deserialize("json", base):
			model.save()
			return base;
	
	def save(self, base):
		base.save()
		return base;
	
	def delete(self, baseId ):
		errMsg = "Failed to delete Base from db using id " + str(baseId)
		
		try:
			base = Base.objects.get(id=baseId)
			base.delete()
			return True
		except Base.DoesNotExist:
			raise ProcessingError("Base with id " + str(baseId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Base.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Base from db")
		except Exception:
			return None;
		
