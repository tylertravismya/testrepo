from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from demo.models.Alley import Alley
from demo.models.League import League
from demo.models.Tournament import Tournament
from demo.models.Lane import Lane
from demo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Alley
#
# @author 
#
#======================================================================

#======================================================================
# Class AlleyDelegate Declaration
#======================================================================
class AlleyDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, alleyId ):
		try:	
			alley = Alley.objects.filter(id=alleyId)
			return alley.first();
		except Alley.DoesNotExist:
			raise ProcessingError("Alley with id " + str(alleyId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, alley):
		for model in serializers.deserialize("json", alley):
			model.save()
			return model;

	def create(self, alley):
		alley.save()
		return alley;

	def saveFromJson(self, alley):
		for model in serializers.deserialize("json", alley):
			model.save()
			return alley;
	
	def save(self, alley):
		alley.save()
		return alley;
	
	def delete(self, alleyId ):
		errMsg = "Failed to delete Alley from db using id " + str(alleyId)
		
		try:
			alley = Alley.objects.get(id=alleyId)
			alley.delete()
			return True
		except Alley.DoesNotExist:
			raise ProcessingError("Alley with id " + str(alleyId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Alley.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Alley from db")
		except Exception:
			return None;
		
	def addLeagues( self, alleyId, leaguesIds ):
		# lazy importing avoids circular dependencies
		from demo.delegates.LeagueDelegate import LeagueDelegate

		errMsg = "Failed to add elements " + str(leaguesIds) + " for Leagues on Alley"

		try:
			# get the Alley
			alley = self.get( alleyId ).first()
				
			# split on a comma with no spaces
			idList = leaguesIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the League		
				league = LeagueDelegate().get(id).first();	
				# add the League
				alley.leagues.add(league)
				
			# save it		
			alley.save()
			
			# reload and return the appropriate version
			return self.get( alleyId );
		except Alley.DoesNotExist:
			raise ProcessingError(errMsg + " : Alley with id " + str(alleyId) + " does not exist.")
		except League.DoesNotExist:
			raise ProcessingError(errMsg + " : League does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeLeagues( self, alleyId, leaguesIds ):
		# lazy importing avoids circular dependencies
		from demo.delegates.LeagueDelegate import LeagueDelegate

		errMsg = "Failed to remove elements " + str(leaguesIds) + " for Leagues on Alley"

		try:
			# get the Alley
			alley = self.get( alleyId ).first()
				
			# split on a comma with no spaces
			idList = leaguesIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the League		
				league = LeagueDelegate().get(id).first();	
				# add the League
				alley.leagues.remove(league)
				
			# save it		
			alley.save()
			
			# reload and return the appropriate version
			return self.get( alleyId );
		except Alley.DoesNotExist:
			raise ProcessingError(errMsg + " : Alley with id " + str(alleyId) + " does not exist.")
		except League.DoesNotExist:
			raise ProcessingError(errMsg + " : League does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addTournaments( self, alleyId, tournamentsIds ):
		# lazy importing avoids circular dependencies
		from demo.delegates.TournamentDelegate import TournamentDelegate

		errMsg = "Failed to add elements " + str(tournamentsIds) + " for Tournaments on Alley"

		try:
			# get the Alley
			alley = self.get( alleyId ).first()
				
			# split on a comma with no spaces
			idList = tournamentsIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Tournament		
				tournament = TournamentDelegate().get(id).first();	
				# add the Tournament
				alley.tournaments.add(tournament)
				
			# save it		
			alley.save()
			
			# reload and return the appropriate version
			return self.get( alleyId );
		except Alley.DoesNotExist:
			raise ProcessingError(errMsg + " : Alley with id " + str(alleyId) + " does not exist.")
		except Tournament.DoesNotExist:
			raise ProcessingError(errMsg + " : Tournament does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeTournaments( self, alleyId, tournamentsIds ):
		# lazy importing avoids circular dependencies
		from demo.delegates.TournamentDelegate import TournamentDelegate

		errMsg = "Failed to remove elements " + str(tournamentsIds) + " for Tournaments on Alley"

		try:
			# get the Alley
			alley = self.get( alleyId ).first()
				
			# split on a comma with no spaces
			idList = tournamentsIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Tournament		
				tournament = TournamentDelegate().get(id).first();	
				# add the Tournament
				alley.tournaments.remove(tournament)
				
			# save it		
			alley.save()
			
			# reload and return the appropriate version
			return self.get( alleyId );
		except Alley.DoesNotExist:
			raise ProcessingError(errMsg + " : Alley with id " + str(alleyId) + " does not exist.")
		except Tournament.DoesNotExist:
			raise ProcessingError(errMsg + " : Tournament does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
	def addLanes( self, alleyId, lanesIds ):
		# lazy importing avoids circular dependencies
		from demo.delegates.LaneDelegate import LaneDelegate

		errMsg = "Failed to add elements " + str(lanesIds) + " for Lanes on Alley"

		try:
			# get the Alley
			alley = self.get( alleyId ).first()
				
			# split on a comma with no spaces
			idList = lanesIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Lane		
				lane = LaneDelegate().get(id).first();	
				# add the Lane
				alley.lanes.add(lane)
				
			# save it		
			alley.save()
			
			# reload and return the appropriate version
			return self.get( alleyId );
		except Alley.DoesNotExist:
			raise ProcessingError(errMsg + " : Alley with id " + str(alleyId) + " does not exist.")
		except Lane.DoesNotExist:
			raise ProcessingError(errMsg + " : Lane does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeLanes( self, alleyId, lanesIds ):
		# lazy importing avoids circular dependencies
		from demo.delegates.LaneDelegate import LaneDelegate

		errMsg = "Failed to remove elements " + str(lanesIds) + " for Lanes on Alley"

		try:
			# get the Alley
			alley = self.get( alleyId ).first()
				
			# split on a comma with no spaces
			idList = lanesIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Lane		
				lane = LaneDelegate().get(id).first();	
				# add the Lane
				alley.lanes.remove(lane)
				
			# save it		
			alley.save()
			
			# reload and return the appropriate version
			return self.get( alleyId );
		except Alley.DoesNotExist:
			raise ProcessingError(errMsg + " : Alley with id " + str(alleyId) + " does not exist.")
		except Lane.DoesNotExist:
			raise ProcessingError(errMsg + " : Lane does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
