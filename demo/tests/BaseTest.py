import datetime

from django.test import TestCase
from django.utils import timezone
from demo.models.Base import Base
from demo.delegates.BaseDelegate import BaseDelegate

 #======================================================================
# 
# Encapsulates data for model Base
#
# @author 
#
#======================================================================

#======================================================================
# Class BaseTest Declaration
#======================================================================
class BaseTest (TestCase) :
	def test_crud(self) :
		base = Base()
		
		delegate = BaseDelegate()
		responseObj = delegate.create(base)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


