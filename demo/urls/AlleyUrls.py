from django.urls import path
from demo.views import AlleyView

urlpatterns = [
    path('', AlleyView.index, name='index'),
	path('create', AlleyView.get, name='create'),
	path('get/<int:alleyId>/', AlleyView.get, name='get'),
	path('save', AlleyView.save, name='save'),
	path('getAll', AlleyView.getAll, name='getAll'),
	path('delete/<int:alleyId>/', AlleyView.delete, name='delete'),
	path('addLeagues/<int:alleyId>/<LeaguesIds>/', AlleyView.addLeagues, name='addLeagues'),
	path('removeLeagues/<int:alleyId>/<LeaguesIds>/', AlleyView.removeLeagues, name='removeLeagues'),
	path('addTournaments/<int:alleyId>/<TournamentsIds>/', AlleyView.addTournaments, name='addTournaments'),
	path('removeTournaments/<int:alleyId>/<TournamentsIds>/', AlleyView.removeTournaments, name='removeTournaments'),
	path('addLanes/<int:alleyId>/<LanesIds>/', AlleyView.addLanes, name='addLanes'),
	path('removeLanes/<int:alleyId>/<LanesIds>/', AlleyView.removeLanes, name='removeLanes'),
]
