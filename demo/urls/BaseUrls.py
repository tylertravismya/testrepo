from django.urls import path
from demo.views import BaseView

urlpatterns = [
    path('', BaseView.index, name='index'),
	path('create', BaseView.get, name='create'),
	path('get/<int:baseId>/', BaseView.get, name='get'),
	path('save', BaseView.save, name='save'),
	path('getAll', BaseView.getAll, name='getAll'),
	path('delete/<int:baseId>/', BaseView.delete, name='delete'),
]
