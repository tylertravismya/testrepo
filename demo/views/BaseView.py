import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from demo.delegates.BaseDelegate import BaseDelegate

 #======================================================================
# 
# Encapsulates data for View Base
#
# @author 
#
#======================================================================

#======================================================================
# Class BaseView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Base index.")

def get(request, baseId ):
	delegate = BaseDelegate()
	responseData = delegate.get( baseId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	base = json.loads(request.body)
	delegate = BaseDelegate()
	responseData = delegate.createFromJson( base )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	base = json.loads(request.body)
	delegate = BaseDelegate()
	responseData = delegate.save( base )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, baseId ):
	delegate = BaseDelegate()
	responseData = delegate.delete( baseId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = BaseDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

