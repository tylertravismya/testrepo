import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from demo.delegates.AlleyDelegate import AlleyDelegate

 #======================================================================
# 
# Encapsulates data for View Alley
#
# @author 
#
#======================================================================

#======================================================================
# Class AlleyView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Alley index.")

def get(request, alleyId ):
	delegate = AlleyDelegate()
	responseData = delegate.get( alleyId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	alley = json.loads(request.body)
	delegate = AlleyDelegate()
	responseData = delegate.createFromJson( alley )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	alley = json.loads(request.body)
	delegate = AlleyDelegate()
	responseData = delegate.save( alley )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, alleyId ):
	delegate = AlleyDelegate()
	responseData = delegate.delete( alleyId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = AlleyDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addLeagues( request, alleyId, LeaguesIds ):
	delegate = AlleyDelegate()
	responseData = delegate.addLeagues( alleyId, LeaguesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeLeagues( request, alleyId, LeaguesIds ):
	delegate = AlleyDelegate()
	responseData = delegate.removeLeagues( alleyId, LeaguesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addTournaments( request, alleyId, TournamentsIds ):
	delegate = AlleyDelegate()
	responseData = delegate.addTournaments( alleyId, TournamentsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeTournaments( request, alleyId, TournamentsIds ):
	delegate = AlleyDelegate()
	responseData = delegate.removeTournaments( alleyId, TournamentsIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addLanes( request, alleyId, LanesIds ):
	delegate = AlleyDelegate()
	responseData = delegate.addLanes( alleyId, LanesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeLanes( request, alleyId, LanesIds ):
	delegate = AlleyDelegate()
	responseData = delegate.removeLanes( alleyId, LanesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

